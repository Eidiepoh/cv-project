import './App.scss';
import { BrowserRouter, Routes, Route} from 'react-router-dom';
import InnerPage from './pages/Inner/InnerPage';
import Home from './pages/Home/Home';
import ExperienceComponent from './pages/Inner/innerPageComponents/ExperienceComponent'


function App() {
  return (
    <BrowserRouter>
        <Routes>
            <Route path='/' element={<Home/>}/>
            <Route path='inner' element={<InnerPage/>}/>
            <Route id='testnav' element={<ExperienceComponent/>}/>

          
        </Routes>
    </BrowserRouter>
  );
}

export default App;

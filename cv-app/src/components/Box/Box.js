import './box.scss';

const Box = ({title, content}) => {
    return (
        <div className='box'>
            <h1 className="box-title">{title ? title : ''}</h1>
            <div className="box-content">
                <div>{content ? content : content}</div>
            </div>
        </div>
    )
}

export default Box;
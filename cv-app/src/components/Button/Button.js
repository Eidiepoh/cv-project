import { useState, useEffect } from 'react';
import './button.scss';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faChevronLeft, faBars, faChevronUp } from '@fortawesome/free-solid-svg-icons'
library.add( faChevronLeft, faBars, faChevronUp);


const Button = ({icon, text, children}) => {
    const [textStatus, setTextStatus] = useState('noText');
    useEffect(() => { 
        if(text || children) {
            setTextStatus('textExists')
        } else {
            setTextStatus('noText')
        }
    },[text, children])

    return(
        <button className={`button ${textStatus}`}>
        {icon ? <span className='button-icon'>{icon}</span> : ''}
        <span className="button-text">{children ? children : text}</span>
        </button>
    )
}

export default Button;

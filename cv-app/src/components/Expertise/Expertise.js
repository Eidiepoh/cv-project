import './expertise.scss';

const Expertise = ({data}) => {
        return(
            <div className="expertise">
                {data.map(item => {
                return (
                    <div className="expertise-container" 
                    key={item.info.company + '-' + item.date}>
                        <div className="expertise-container-company">
                            <h5>{item.info.company}</h5>
                            <div>{item.date}</div>
                        </div>
                        <div className="expertise-container-job">
                            <h4>{item.info.job}</h4>
                            <p>{item.info.description}</p>
                        </div>
                    </div>
                )
                })}
            </div>
        )
}

export default Expertise;
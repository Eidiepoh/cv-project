import './feedback.scss';
import Info from '../Info/Info';

const Feedback = ({data}) => {
    const dataObject = data.map((data, i) => ({data: data, id: i}))
    return(
        <div>
            {dataObject.map(item => {
             return (
                <div className="feedback"
                key={item.id}>
                <Info text={item.data.feedback}/>
                <div className="feedback-author">
                    <img src={item.data.reporter.photoUrl} className="feedback-author image" alt="User avatar"/> 
                    <div className="feedback-author user">
                        <span>{`${item.data.reporter.name}, `} 
                            <a href={item.data.reporter.citeUrl} className="feedback-author-user website">
                                {item.data.reporter.citeUrl.slice(item.data.reporter.citeUrl.indexOf('www.')+4)}
                            </a>
                        </span>
                    </div>
                </div>
                
                </div>
             )
            })}
        </div>
    )
}

export default Feedback;

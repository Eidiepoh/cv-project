import './Info.scss';

const Info = ({text}) => {
    return (
        <p className="info-text">{text ? text : ''}</p>
    )
}

export default Info;

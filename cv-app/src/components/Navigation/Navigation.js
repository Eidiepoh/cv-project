import './navigation.scss';
import { FaUser, FaGraduationCap, FaPen, FaSuitcase, FaLocationArrow, FaComment } from 'react-icons/fa';
import { Link } from "react-scroll";

const Navigation = () => {
    const option = {
        duration: 500,
        smooth: true,
        spy: true,
        activeClass:"nav-link",
        hashSpy:true,
        offset: -50
    }
    
    return(
        <nav className="nav">
            <ul className='nav-list'>               
                    <Link to="about" {...option}>
                        <li className="nav-list-item" >
                                <FaUser  size={15} title="About me"/> 
                                <span className="nav-list-item-text">About me</span>
                        </li>
                    </Link>
                    
                    <Link to="education" {...option}>
                        <li className="nav-list-item">
                            <FaGraduationCap  size={18} title="Education"/>  
                            <span className="nav-list-item-text">Education</span>
                        </li>
                    </Link>
                    <Link to="experience" {...option}>
                        <li className="nav-list-item">
                            <FaPen  size={15} title="Experience"/>  
                            <span className="nav-list-item-text">Experience</span>
                        </li>
                    </Link>
                    <Link to="portfolio" {...option}>
                        <li className="nav-list-item" >
                            <FaSuitcase  size={15} title="Portfolio"/>      
                            <span className="nav-list-item-text">Portfolio</span>
                        </li>
                    </Link>
                    <Link to="contacts" {...option}>
                        <li className="nav-list-item">
                            <FaLocationArrow  size={15} title="Contacts"/>      
                            <span className="nav-list-item-text">Contacts</span>
                        </li>
                    </Link>
                    <Link to="feedback" {...option} offset={-100}>
                        <li className="nav-list-item">
                            <FaComment  size={15} title="Feedback"/>       
                            <span className="nav-list-item-text">Feedback</span>
                        </li>
                    </Link>
            </ul>
        </nav>
    )
}

export default Navigation;
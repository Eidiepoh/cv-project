import { useState } from 'react';
import './panel.scss';
import PhotoBox from '../PhotoBox/PhotoBox';
import Navigation from '../Navigation/Navigation';
import Button from '../Button/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

const Panel = () => {
    const [panelStatus, setPanelStatus] = useState(true)

    return (
         <div className={`panel-box-container ${panelStatus ? '' : 'panel-hide'}`}>
            <div className={`panel-box ${panelStatus ? '' : 'panel-hide'}`}>
            <div className={`panel`}>
                <div className="panel-photobox-navigation">
                    <PhotoBox 
                    avatar="http://avatars0.githubusercontent.com/u/246180?v=4"
                    name='woo jo'/>
                    <Navigation/>
                </div>
                <div className="panel-photobox-button">
                    <Link to='/' style={{ textDecoration: 'none' }}>
                        <Button icon={ <FontAwesomeIcon icon="fa-solid fa-chevron-left" />}>
                            <span className="button-text-toggler">Go back</span>
                        </Button>
                    </Link>
                </div>            
            </div>
            <span className="menu-toggler" onClick={() => setPanelStatus(!panelStatus)}>
                <Button icon={ <FontAwesomeIcon icon="fa-bars" />}/>
            </span>
         
        </div>
        </div>
     
    )
}

export default Panel;

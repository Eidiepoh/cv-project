import { useState, useEffect } from 'react';
import './photobox.scss';

const PhotoBox = ({name, title, description, avatar}) => {
    const [size, setSize] = useState(true)

    useEffect(() => {
        if(title && description) {
            setSize(true)
        } else {
            setSize(false)
        }
    },[title,description])

    return (
        <div className="photobox">
            <img className={`photobox-image ${size ? 'large' : 'small'} rezize`} src={avatar} alt={'avatar'}/>
            <h1 className={`photobox-name ${size ? 'large' : 'small'}`}>{name}</h1>
            {!size ? '' : <h2 className="photobox-title">{title}</h2>}
            {!size ? '' : <p className="photobox-description">{description}</p> }
        </div>
    )
}

export default PhotoBox;

import { useEffect } from 'react';
import './filter.scss';

const Filter = ({ setFilteredData, allData, activeType, setActiveType}) => {
    useEffect(() => {
        if(activeType === 'All') {
            setFilteredData(allData);
            return;
        }
        const filtered = allData.filter((data) => data.type === activeType);
        setFilteredData(filtered);
    },[activeType, allData, setFilteredData])

    return(
        <div className="filter-container">
            <button 
            className={`filter-container-button ${activeType === 'All' ? 'active' : ''}`}
                onClick={() => setActiveType('All')}>
                All
            </button> /
            <button
            className={`filter-container-button ${activeType === 'Code' ? 'active' : ''}`}
                onClick={() => setActiveType('Code')}>
                Code
            </button> /
            <button
            className={`filter-container-button ${activeType === 'Ui' ? 'active' : ''}`}
                onClick={() => setActiveType('Ui')}>
                UI
            </button>
        </div>
    )
}

export default Filter;
import './portfolio.scss';
import { useState } from 'react';
import PortfolioInfo from './PortfolioInfo/PortfolioInfo';
import Filter from './Filter/Filter';
import portfolioData from './portfolioData/portfolioData';
import { motion, AnimatePresence } from 'framer-motion'


const Portfolio = () => {
    const allData = portfolioData;
    const [filteredData, setFilteredData] = useState([])
    const [activeType, setActiveType] = useState('All')

    return (
        <div>
           <Filter allData={allData} 
                    setFilteredData={setFilteredData} 
                    activeType={activeType}
                    setActiveType={setActiveType}/>
        <motion.div layout className="portfolio-container">
       <AnimatePresence>
       {filteredData.map(data => {
            return  (
                <PortfolioInfo 
                title={data.title} 
                text={data.text} 
                url={data.url}
                dataImage={data.card}
                key={allData.indexOf(data)}/>
            )
        })}
       </AnimatePresence>
        </motion.div>
        </div>
    )
}

export default Portfolio;

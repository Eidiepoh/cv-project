import './portfolioInfo.scss';
import { motion } from 'framer-motion';

const PortfolioInfo = ({title, text, url, dataImage}) => {

    return(
            <motion.div layout
            initial={{ opacity: 0, scale: 0 }}
            animate={{ opacity: 1, scale: 1 }}
            exit={{ opacity: 0, scale: 0 }}
            transition={{dutaion: 1}}
            className="portfolio-info">
            <div className="portfolio-info-image">
                <img src={dataImage} alt={`${title} project`}/>
            </div>
            <div className="portfolio-info-data">
                <h3>{title}</h3>
                <p>{text}</p>
                <a href={url}>View source</a>
            </div>
        </motion.div>

    )
}

export default PortfolioInfo;
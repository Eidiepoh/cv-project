const cardCode = require('../../../assets/images/card_1.jpg');
const cardUi = require('../../../assets/images/card_2.png');

const portfolioData = [
    {
        title: "Some text",
        text: "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo",
        url: "https://somesite.com",
        type: 'Code',
        card: cardCode
    },
    {
        title: "Some text",
        text: "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo",
        url: "https://somesite.com",
        type: 'Ui',
        card: cardUi
    },
    {
        title: "Some text",
        text: "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo",
        url: "https://somesite.com",
        type: 'Code',
        card: cardCode
    },
    {
        title: "Some text",
        text: "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo",
        url: "https://somesite.com",
        type: 'Ui',
        card: cardUi
    },
]

export default portfolioData;

import './timeline.scss';
import Info from '../Info/Info'

const TimeLine = ({data}) => {

    return (
        <div className="timeline-container">
            
            {data.map(info => {
                return (
                <div className="timeline-container-info"
                key={info.date + '-' + info.title}>
                <div className="timeline-container-info-line"></div>
                
                    <div className="timeline-container-info-date">
                        {info.date}
                    </div>
                    <div className="timeline-container-info-box">
                        <div className="timeline-container-info-box-title">
                            <h3>{info.title}</h3>
                        </div>
                        <div className="timeline-container-info-box-flag"></div>
                        <Info text={info.text}/>
                    </div>
                </div>
                )
            })}
        </div>
    )
}

export default TimeLine;

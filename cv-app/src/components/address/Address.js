import './address.scss';
import { FaPhoneAlt, FaLinkedin, FaFacebookF, FaSkype } from 'react-icons/fa';
import { BsFillEnvelopeFill } from 'react-icons/bs';

const Address = () => {
    return (
        <div className="address">        
            <ul className='address-list'>
                <li className="address-list-item">
                    <div className="address-list-item-icon">
                        <FaPhoneAlt  size={30}/>
                    </div>
                    <div className="address-list-item-contact">
                        <a href="tel:+995555351415"><h3>+995 555 35 14 15</h3></a>
                    </div>
                </li>

                <li className="address-list-item">
                    <div className="address-list-item-icon">
                        <BsFillEnvelopeFill  size={30}/>  
                    </div>
                    <div className="address-list-item-contact">
                        <h3 onClick={() => window.location = 'mailto:tornike.dzmanashvili@gmail.com'}
                            className="address-list-item-contact-action">
                            tornike.dzmanashvili@gmail.com
                        </h3>
                    </div>
                </li>

                <li className="address-list-item">
                    <div className="address-list-item-icon"> 
                        <FaLinkedin size={30}/>
                    </div>
                    <div className="address-list-item-contact">
                        <h3>Twitter</h3>
                        <p>https://www.linkedin.com/in/tornike-dzmanashvili-472141230</p>
                    </div>
                </li>

                <li className="address-list-item">  
                    <div className="address-list-item-icon">
                        <FaFacebookF size={40}/>
                    </div> 
                    <div className="address-list-item-contact">
                        <h3>Facebook</h3>
                        <p>facebook.com</p>
                    </div>
                </li>

                <li className="address-list-item">
                    <div className="address-list-item-icon">
                        <FaSkype size={40}/>
                    </div>
                    <div className="address-list-item-contact">
                        <h3>Skype</h3>
                        <p>toko19961</p>
                    </div>
                </li>
            </ul>     
        </div>
    )
}

export default Address;

import './innerPage.scss';
import Panel from '../../components/Panel/Panel';
import InnerContent from './innerContent/InnerContent';
import Button from '../../components/Button/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from "react-scroll";
import { animateScroll as scroll } from 'react-scroll';

const InnerPage = () => {
    return (
       <div className="inner-page-container">
            <div className="inner-page-panel">
                <Panel/>
            </div>

            <div className='inner-page-content'>
                <InnerContent/> 
            </div>
           <div className='go-up-button'>
                <Link onClick={() => scroll.scrollToTop()} duration={150} smooth={true}>
                    <Button icon={ <FontAwesomeIcon icon="fa-solid fa-chevron-up" />}/>
                </Link>
           </div>  
       </div> 
    )
}

export default InnerPage;
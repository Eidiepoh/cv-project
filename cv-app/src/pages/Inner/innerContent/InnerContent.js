import './innerContent.scss';
import Box from '../../../components/Box/Box';
import Portfolio from '../../../components/Portfolio/Portfolio';
import Address from '../../../components/address/Address';
import AboutMeComponent from '../innerPageComponents/AboutMeComponent';
import EducationComponent from '../innerPageComponents/EducationComponent';
import ExperienceComponent from '../innerPageComponents/ExperienceComponent';
import FeedbackComponent from '../innerPageComponents/FeedbackComponent';


const InnerContent = () => {
    return (
        <div className='inner-content-container'>
            <div name="about">
                <AboutMeComponent/>
            </div>
            <div name="education">
                <EducationComponent/>
            </div>
            <div name='experience'>
                <ExperienceComponent/>
            </div>
            <div name='portfolio'>
                <Box title='Portfolio' content={<Portfolio/>}/>
            </div>
            <div name='contacts'>
                <Box title='Contacts' content={<Address/>}/>
            </div>
            <div name='feedback'>
                <FeedbackComponent/>
            </div>
        </div>
    )
}

export default InnerContent;

import Box from '../../../components/Box/Box';

const AboutMeComponent = () => {
    return (
        <Box title='About me'
        content='Lorem ipsum dolor sit amet, consectetuer adipiscing elit.' />
    )
}

export default AboutMeComponent;

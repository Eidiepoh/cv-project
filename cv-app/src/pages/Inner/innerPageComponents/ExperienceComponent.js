import Box from '../../../components/Box/Box';
import Expertise from '../../../components/Expertise/Expertise';

const ExperienceComponent = () => {
    return (
        <div>
            <Box title='Experience' 
        content={<Expertise 
            data={[ { date: '2013-2014', info: { company: 'Google', job: 'Front-end developer / php programmer', description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor' } }, { date: '2012', info: { company: 'Twitter', job: 'Web developer', description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor' } } ]} />}/>
        </div>

         )
}

export default ExperienceComponent;
